import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

public class Pendu extends JFrame
{
    // The csv file containing all the words.
    static File hangmanFile = new File("data/dico.csv");

    // Words to be used for the game
    static List<String> wordsToPickForGame = new ArrayList<String>();

    static List<List<String>> records = new ArrayList<>();

    // Pseudo random number generator
    static Random psrng = new Random();

    // Gameplay bookkeeping variables
    static String rand_word; // Stores the chosen word

    int miss_chance; //setting of miss chances (a voir si je peut pas regler autrement les display image en avec la boucle)



    private void CreateWordList()
    {
        try (BufferedReader br = new BufferedReader(new FileReader(hangmanFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(", ");
                records.add(Arrays.asList(values));

            }
            System.out.println(records);
            System.out.println("La liste de mots contients: " + records.get(0).size() + " mots possible");

            wordsToPickForGame = records.get(0); // get 0 prend la list a l'interieur du premier objet recors et cree une list pour la stocker dans wordtopicksforgame

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private void PenduGui(ImageIcon image)
    {
        JFrame frameHangMan = new JFrame();
        //Jframe gestion for the game
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Pendu");

        JLabel label = new JLabel(image); //seting image for the game
        JScrollPane scrollPane = new JScrollPane(label);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(scrollPane, BorderLayout.CENTER);
        setVisible(true);
        setBounds(200,200,500,500);
        SwingUtilities.updateComponentTreeUI(frameHangMan); // refresh §??
    }


    public static void main( String[] args )
    {
        Pendu guiTest = new Pendu();
        guiTest.CreateWordList(); // run the method to get the word for the game


        // Get a random word from our list of words
        int index = psrng.nextInt(wordsToPickForGame.size());


        rand_word = wordsToPickForGame.get(index);
        System.out.println("Le mots caché est: " + rand_word); // Pour Tricher

        //on cree une method char pour stocker chaque lettre du user  et faire une oucle si la lettre n'est pas dans le mots
        char[] hidden_word; //initiating the hindden word
        Scanner keyboard = new Scanner(System.in); // read the file of words
        String user_guess; // proposition of the user
        int miss_chance = 0;
        char[] missed = new char[7];
        boolean letter_found = false, solved = false; //condition of the game

        //rand_word = word_list[ (int)(Math.random() * word_list.length) ];
        hidden_word = new char[rand_word.length()];
        for ( int i = 0; i < rand_word.length(); i++ )
        {
            hidden_word[i] = '_';
        }


        while (miss_chance < 6 && ! solved)
        {
            ImageIcon image = new ImageIcon("images/Hangman_"+miss_chance+".png");

            guiTest.PenduGui(image); //run the testing for the gui image of the game

            System.out.println( "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n" );
            System.out.println( "Vous avez inside" + (6 - miss_chance) + " Tours restants." );
            System.out.print( "Mots:\t" );
            for ( int i = 0; i < rand_word.length(); i++ ) {
                System.out.print( hidden_word[i] + " " );
            }
            System.out.print("\nLettres proposés: ");
            for ( int i = 1; i < missed.length; i++ ) {
                System.out.print( missed[i] );

            }

            System.out.print( "\nProposition: " );
            user_guess = keyboard.next();
            letter_found = false;

            for ( int i = 0; i < rand_word.length(); i++ ) {
                if ( user_guess.charAt(0) == rand_word.charAt(i) ) {
                    hidden_word[i] = rand_word.charAt(i);
                    letter_found = true;
                }
            }
            if (!letter_found) {
                miss_chance++;

                missed[miss_chance] = user_guess.charAt(0);
            }

            int hidden_count = 0;
            for ( int i = 0; i < rand_word.length(); i++ ) {
                if ( '_' == hidden_word[i] )
                    hidden_count++;
            }
            if (hidden_count > 0)
                solved = false;
            else
                solved = true;
        }

        System.out.println( "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n" );
        System.out.println( "Vous avez outside" + (6 - miss_chance) + " Tours restants." );
        System.out.print( "Mots:\t" );
        for ( int i = 0; i < rand_word.length(); i++ ) {
            System.out.print( hidden_word[i] + " " );
        }
        System.out.print("\nProposition: ");
        for ( int i = 0; i < missed.length; i++ ) {
            System.out.print( missed[i] );
        }


        if (solved)
            System.out.println( "Bravo! vous avez trouvez le bon mot : " + rand_word );
        else
            System.out.println( "\n\nLe mots étais..." + rand_word );
    }
}
