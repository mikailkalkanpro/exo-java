import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class JSONManage implements ActionListener {


    private String path ="data/myJson.json";
    public static void main(String[] args) {
        JSONManage jsm = new JSONManage();
        //Voiture v1 = new Voiture("ferrari","411Spider","rouge","essance",10,"bon");
        //jsm.ecrireJson(v1);
        jsm.afficheJson();
    }


    public JSONArray afficheJson(){
        JSONArray json = this.lireJson();
        return json;
    }
    //lecture du json
    public JSONArray lireJson(){
        JSONParser parser = new JSONParser();
        JSONArray jsonArray = new JSONArray();
        try {
            FileReader fr = new FileReader(this.path);
            Object ob = parser.parse(fr);//parsing du json
            System.out.println("ob = "+ob);
            jsonArray = (JSONArray) ob; // on cast en JsonArray objet parser
            return jsonArray;
//            jsonArray.forEach(
//                    contact -> {
//                        JSONObject myContact = (JSONObject) contact;
//                        System.out.println(myContact);
//                        JSONObject personne =(JSONObject) myContact.get("contact");
//                        System.out.println(personne);
//                    }
//            );
        }catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        } catch (ParseException e) {
            return jsonArray;
            //throw new RuntimeException(e.getMessage());
        }

        return jsonArray;
    }

    //ecriture du Json
    public void ecrireJson(Voiture voiture){
        try{
//            JSONArray rootContact = new JSONArray();//construction du tableau json qui vas contenir le list
            JSONArray rootContact = this.afficheJson();//construction du tableau json qui vas contenir le list
            rootContact.add(voiture.toJson());
            FileWriter fw = new FileWriter(this.path);//je prépare l'écriture du fichier
            fw.write(rootContact.toJSONString());//j'écris dans le fichier grace au json array transformer en string
            fw.flush();//je vide mon objet
        }catch (IOException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        //ecrireJson();
    }
}