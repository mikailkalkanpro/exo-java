import org.json.simple.JSONObject;

public class Voiture {
    private String brand;
    private String model;
    private String color;
    private String carburation;
    private int age;
    private String state;


    public Voiture() {
    }

    public Voiture(String brand, String model, String color, String carburation, int age, String state) {
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.carburation = carburation;
        this.age = age;
        this.state = state;
    }


    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCarburation() {
        return carburation;
    }

    public void setCarburation(String carburation) {
        this.carburation = carburation;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Voiture{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", carburation='" + carburation + '\'' +
                ", age=" + age +
                ", state=" + state +
                '}';
    }

    public JSONObject toJson(){
        JSONObject voiture = new JSONObject(); // creation de l'objet json de contact
        voiture.put("brand",this.brand);// ajout de proprieter et de valeur
        voiture.put("model",this.model);
        voiture.put("color", this.color);
        voiture.put("carburation", this.carburation);
        voiture.put("age", this.age);
        voiture.put("state", this.state);


        JSONObject voitureList = new JSONObject();//create d'un objet json de container
        voitureList.put("voiture",voiture);

        return voiture;
    }
}