public class Monnaie {
    private double usd = 1.09;
    private double franc = 6.56;
    private double pesos = 19.67;
    private double rouble = 59.53;




    public double convertirDollar(double euroUS){
        double resultat;

        resultat = (euroUS*this.usd);

        return resultat;
    }
    public double convertirFranc(double euroUS){
        double resultat;

        resultat = (euroUS*this.franc);

        return resultat;
    }
    public double convertirPesos(double euroUS){
        double resultat;

        resultat = (euroUS*this.pesos);

        return resultat;
    }
    public double convertirRouble(double euroUS){
        double resultat;

        resultat = (euroUS*this.rouble);

        return resultat;
    }


}