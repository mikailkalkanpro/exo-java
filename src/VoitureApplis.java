import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static java.lang.Integer.parseInt;

public class VoitureApplis{
    JLabel label;
    JTable table;

    final JFrame frame = new JFrame("Fenêtre");


    private JLabel name;
    private JTextField tname;
    private DefaultTableModel defaultModel;
    private String path ="data/myJson.json";

    public static void main(String[] args) {
        new VoitureApplis();

    }

    public VoitureApplis(){
        getTable();
        parserJson();


        JPanel panelMenu = new JPanel();//seting Panel
        JPanel panelBody = new JPanel();
        JPanel panelForm = new JPanel();


        panelMenu.setBackground(Color.LIGHT_GRAY); //seting Color
        panelBody.setBackground(Color.DARK_GRAY);
        panelForm.setBackground(Color.LIGHT_GRAY);


        panelMenu.setLayout(new BoxLayout(panelMenu, BoxLayout.X_AXIS));//seting Boxlayout
        panelBody.setLayout(new BoxLayout(panelBody, BoxLayout.X_AXIS));
        panelForm.setLayout(new BoxLayout(panelForm, BoxLayout.Y_AXIS));



        JButton btn2 = new JButton("Ajout voiture");
        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panelForm.setVisible(true);

            }
        });

        //FORM LABEL FIELD
        JLabel titleLabel = new JLabel("Ajouter Votre Voiture");
        titleLabel.setFont(new Font("Arial", Font.PLAIN, 20));

        JLabel marqueLabel = new JLabel("Marque Voiture");
        marqueLabel.setFont(new Font("Arial", Font.PLAIN,15));
        JTextField marque = new JTextField();

        JLabel modelLabel = new JLabel("Couleur");
        modelLabel.setFont(new Font("Arial", Font.PLAIN,15));
        JTextField model = new JTextField();

        JLabel colorLabel = new JLabel("Model");
        colorLabel.setFont(new Font("Arial", Font.PLAIN,15));
        JTextField color = new JTextField();

        JLabel carburantLabel = new JLabel("Carburant");
        colorLabel.setFont(new Font("Arial", Font.PLAIN,15));
        JTextField carburant = new JTextField();

        JLabel ageLabel = new JLabel("Age Voiture");
        ageLabel.setFont(new Font("Arial", Font.PLAIN,15));
        JTextField age = new JTextField(1);

        JLabel stateLabel = new JLabel("Etat Voiture");
        stateLabel.setFont(new Font("Arial", Font.PLAIN,15));
        JTextField state = new JTextField(1);


        JButton btnSubmit = new JButton("Submit");
        btnSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createJSon(marque,model,color,carburant,age,state);
                //frame.repaint();
                //frame.revalidate();
                //panelBody.repaint();
                //panelBody.revalidate();
                //table.repaint();
                //table.revalidate();
                //table.setGridColor(Color.BLUE);

            }
        });
        JButton btnClose = new JButton("Close");
        btnClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panelForm.setVisible(false);
            }
        });




        //Redessiner la frame




        //Ajout des champs au layout
        panelMenu.add(btn2);// menu add
        panelBody.add(table); // body add

        panelForm.add(btnClose);//form add

        panelForm.add(titleLabel);//form add

        panelForm.add(marqueLabel);
        panelForm.add(marque);

        panelForm.add(modelLabel);
        panelForm.add(model);

        panelForm.add(colorLabel);
        panelForm.add(color);

        panelForm.add(carburantLabel);
        panelForm.add(carburant);

        panelForm.add(ageLabel);
        panelForm.add(age);

        panelForm.add(stateLabel);
        panelForm.add(state);

        panelForm.add(btnSubmit);

        panelForm.setVisible(false);

        //frame.repaint();
        //frame.revalidate();
        //panelBody.repaint();
        //panelBody.revalidate();
        //table.repaint();
        //table.revalidate();


        frame.getContentPane().add(BorderLayout.NORTH, panelMenu);
        frame.getContentPane().add(BorderLayout.CENTER, panelBody);
        frame.getContentPane().add(BorderLayout.EAST, panelForm);
        //frame.getContentPane().add(BorderLayout.EAST, btn2);
        //frame.getContentPane().add(BorderLayout.WEST, label2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1200, 900);
        frame.setVisible(true);
    }

    private JTable getTable() {
        String[] colName = { "Brand", "Model", "Color", "Carburation",
                "Age", "State" };
        if (table == null) {
            table = new JTable(defaultModel) {
                public boolean isCellEditable(int nRow, int nCol) {
                    return false;
                }
            };
        }
        DefaultTableModel defaultModel = (DefaultTableModel) table.getModel();
        defaultModel.setColumnIdentifiers(colName);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        return table;
    }

    public JSONArray parserJson(){
        JSONParser parser = new JSONParser(); // creation Parser
        JSONArray jsonArray = new JSONArray(); // creation Array


        try {

            FileReader fReader = new FileReader(this.path);
            Object ob = parser.parse(fReader); // JSON parsing
            jsonArray = (JSONArray) ob; // on cast en JsonArray objet parser


            //data = new Object[jsonArray.size()][6];
            JSONObject objectJson; //init JSon  Object

            DefaultTableModel defaultModel = (DefaultTableModel) table.getModel();


                //Objet to Json objet pour recuperer les bonne cle valeur
                for (int i = 0; i < jsonArray.size(); i++) {
                    //String[] data = new String[7];
                    Object[][] data = new Object[jsonArray.size()][7];

                    //parsing ligne du JsonArray dans le JsonObject
                    objectJson = (JSONObject) jsonArray.get(i);


                    data[i][0] = objectJson.get("brand"); //parsing data into column
                    data[i][1] = objectJson.get("Marque");
                    data[i][2] = objectJson.get("color");
                    data[i][3] = objectJson.get("carburation");
                    data[i][4] = objectJson.get("age");
                    data[i][5] = objectJson.get("state");


                    defaultModel.addRow(data[i]);
                }
                this.defaultModel = defaultModel;
                System.out.println(jsonArray.size());






        }catch (FileNotFoundException e){
            System.out.println("test1");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("test2");
            System.out.println(e.getMessage());
            //throw new RuntimeException(e.getMessage());
        } catch (ParseException e) {
            System.out.println("test3");
            System.out.println(e.getMessage());
            //return jsonArray;
            //throw new RuntimeException(e.getMessage());
        }
        return jsonArray;
    }

    private void createJSon(JTextField marque, JTextField model, JTextField color, JTextField carburant, JTextField age, JTextField state) {
        JSONManage manage = new JSONManage();
        DefaultTableModel defaultModel = (DefaultTableModel) table.getModel();

        Voiture voiture = new Voiture( marque.getText(),model.getText(),color.getText(),carburant.getText(),parseInt(age.getText()),state.getText());

        manage.ecrireJson(voiture);

        defaultModel.setRowCount(0);
        parserJson();
    }



}
